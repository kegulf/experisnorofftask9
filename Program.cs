﻿using ExperisNoroffTask9.model;
using System;
using System.Collections.Generic;

namespace ExperisNoroffTask9 {
    class Program {

        /// <summary>
        ///     Small program that creates a list of animals and tells them to make some noice! 
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args) {

            List<Animal> animals = CreateAnimalList();

            foreach(Animal animal in animals) {

                // Print a new line, then the toString of the animal
                Console.WriteLine($"\n{animal}");

                // Make some noice for OOP
                animal.MakeSound();

                // Donald Trump is a loud-mouth, so he says more than one thing, obv.. 
                if(animal.Name.Equals("Donald Trump")) {
                    for (int i = 0; i < 3; i++) {
                        animal.MakeSound();
                    }
                }
            }
        }

        /// <summary>
        ///     Creates an animal list and fills it with animals
        /// </summary>
        /// <returns>
        ///     A List containing Animals
        /// </returns>
        static List<Animal> CreateAnimalList() {

            List<Animal> animalList = new List<Animal>();

            animalList.Add(new Animal("Fido", 3, "dog", "woof", "gnarl"));
            animalList.Add(new Animal("Kitty", 4, "cat", "meow", "hiss"));
            animalList.Add(new Animal("Dagros", 15, "cow", "mooo"));
            animalList.Add(new Animal(
                "Donald Trump", 73, "lizard", 
                // Sounds
                "Make America great again",
                "I have a great relationship with the blacks.",
                "I will build a great, great wall on our southern border, and I will make Mexico pay for that wall",
                "The beauty of me is that I'm very rich.",
                "It's freezing and snowing in New York--we need global warming!",
                "Grab them by the pussy",
                "I'm their worst nightmare.",
                "I take advantage of the laws of the nation. Because I'm running a company.",
                "I Have never seen a thin person drinking Diet Coke.",
                "I know nothing about the inter workings of Russia."));

            animalList.Add(new Animal("Spot", 5, "dog", "woof", "gnarl"));

            return animalList;

        }
    }
}
