﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExperisNoroffTask9.model {
    class Animal {

        #region Properties

        private string name;
        private int age;
        private string species;
        private List<string> sounds = new List<string>();

        #endregion



        #region Constructors

        /// <summary>
        ///     Default constructor
        /// </summary>
        public Animal() { }



        /// <summary>
        ///     Constructor that takes in the name, age and species of the animal
        /// </summary>
        /// 
        /// <example>
        ///     new Animal("Fido", 3, "dog");
        /// </example>
        /// 
        /// <param name="name">The name of the animal</param>
        /// <param name="age">The age of the animal</param>
        /// <param name="species">The species of the animal</param>
        public Animal(string name, int age, string species) {
            this.name = name;
            this.age = age;
            this.species = species;
        }



        /// <summary>
        ///     Constructor that takes in the name, age, species and the sound of the animal
        /// </summary>
        /// 
        /// <example>
        ///     new Animal("Fido", 3, "dog", "woof");
        /// </example>
        /// 
        /// <param name="name">The name of the animal</param>
        /// <param name="age">The age of the animal</param>
        /// <param name="species">The species of the animal</param>
        /// <param name="sound">The sound the animal makes</param>
        public Animal(string name, int age, string species, string sound) {
            this.name = name;
            this.age = age;
            this.species = species;
            this.sounds.Add(sound);
        }



        /// <summary>
        ///     Constructor that takes in the name, age, species and sounds of the animal
        /// </summary>
        /// 
        /// <example>
        ///     new Animal("Fido", 3, "dog", "woof", "gnarl");
        /// </example>
        /// 
        /// <param name="name">The name of the animal</param>
        /// <param name="age">The age of the animal</param>
        /// <param name="species">The species of the animal</param>
        /// <param name="sounds">The sounds the animal makes</param>
        public Animal(string name, int age, string species, params string[] sounds) {
            this.name = name;
            this.age = age;
            this.species = species;
            this.sounds.AddRange(sounds);
        }

        #endregion 



        #region Behavior

        /// <summary>
        ///     Prints a random sound that the animal makes to the Console.
        /// </summary>
        public void MakeSound() {
            if (sounds.Count != 0) {
                int randIndex = new Random().Next(0, sounds.Count);
                Console.WriteLine(sounds[randIndex]);
            }
            else
                Console.WriteLine($"This {species} is a mute");
        }

        #endregion



        #region Overrides

        override
        public string ToString() {

            // I know this is a bit lazy, and also not fool proof, but I will assume
            // the user is smart enough to not put in null for species and age :laughing:
            if (name == null) return "We have no information about this animal.."; 

            return $"The {species} {name} is {age} years old.";
        }

        #endregion



        #region Access Modifiers

        public string Name { get => name; set => name = value; }
        public int Age { get => age; set => age = value; }
        public string Species { get => species; }
        public List<string> Sounds { get => new List<string>(sounds); }
        
        #endregion

    }
}
